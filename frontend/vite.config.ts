import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import svgr from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svgr({
      svgrOptions: {
        svgo: true,
      },
    }),
    react(),
  ],
  server: { open: "/notes" },
  css: {
    modules: { localsConvention: "camelCaseOnly" },
  },
});
