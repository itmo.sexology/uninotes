import React from "react";
import c from "./style.module.sass";
import MenuItem from "./MenuItem";
import classNames from "classnames";

type MenuBlockProps = {
  title?: string;
  isMain?: boolean;
  children: React.ReactNode;
};

const MenuBlock = ({
  children,
  isMain = false,
  title,
  ...rest
}: MenuBlockProps) => {
  const className = classNames(c.menuBlock, {
    [c.menuMainBlock]: isMain,
  });

  return title ? (
    <div className={className} {...rest}>
      <h3>{title}</h3>
      {children}
    </div>
  ) : (
    <div className={className} {...rest}>
      {children}
    </div>
  );
};

export default React.memo(MenuBlock);
