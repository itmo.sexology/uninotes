# pydantic model for note which contains id as str, type as str, value as str
from pydantic import BaseModel


class Note(BaseModel):
    id: str
    type: str
    value: str
