import React from "react";
import { Navigate } from "react-router";

const Main = () => {
  return (
    <>
      <div className="left-panel"></div>
      <div className="right-panel"></div>
      {Navigate({ to: "/notes" })}
    </>
  );
};

export default React.memo(Main);
