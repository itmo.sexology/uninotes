from flask import Blueprint

heartbeat = Blueprint("heartbeat", __name__)


@heartbeat.route("/", methods=["GET"])
def heartbeat_route():
    return "OK"
