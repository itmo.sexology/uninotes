from flask import Flask

from responses.errors.user import UserExistError
from routes.v1_0.heartbeat import heartbeat

app = Flask("uninotes-backend")
app.register_blueprint(heartbeat, url_prefix="/api/v1.0/heartbeat")


an = UserExistError()
print(an.asdict())

__import__("time").sleep(3)
print(an.asdict())

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=3000, debug=True)
