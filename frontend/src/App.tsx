import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./style/main.sass";
import Main from "./pages/Main";
import Auth from "./pages/Auth";
import Notes from "./pages/Notes";

const App = () => {
  useEffect(() => {
    const body = document.getElementsByTagName("body");
    if (body.length) body[0].dataset.theme = "dark";
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="auth" element={<Auth />} />
        <Route path="notes" element={<Notes />} />
      </Routes>
    </BrowserRouter>
  );
};

export default React.memo(App);
