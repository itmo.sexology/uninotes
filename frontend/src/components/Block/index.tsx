import React from "react";
import c from "./style.module.sass";

type BlockProps = {
  id: number;
  type: string;
  value: string;
};

const Block = ({ id, type, value, ...rest }: BlockProps) => {
  const blockProperties = {
    className: c.block,
    contentEditable: type !== "img",
    src: type === "img" ? value : undefined,
    ...rest,
  };

  switch (type) {
    case "img":
      return <img {...blockProperties} />;

    case "h1":
      return <h1 {...blockProperties}>{value}</h1>;

    case "h2":
      return <h2 {...blockProperties}>{value}</h2>;

    case "h3":
      return <h3 {...blockProperties}>{value}</h3>;

    case "p":
      return <p {...blockProperties}>{value}</p>;

    default:
      return <span {...blockProperties}>{value}</span>;
  }
};

export default Block;
