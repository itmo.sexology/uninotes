import React from "react";
import c from "./style.module.sass";

type MenuItemProps = {
  icon: React.FC<React.SVGProps<SVGSVGElement>>;
  children: React.ReactNode;
};

const MenuItem = ({ icon, children, ...rest }: MenuItemProps) => {
  const Icon = icon;

  return (
    <span className={c.item} {...rest}>
      <span className={c.icon}>
        <Icon />
      </span>
      <p className={c.text}>{children}</p>
    </span>
  );
};

export default React.memo(MenuItem);
