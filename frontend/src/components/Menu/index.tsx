import React from "react";
import c from "./style.module.sass";

import Block from "./MenuBlock";
import Item from "./MenuItem";
import { ReactComponent as SearchIcon } from "../../media/svg/search.svg";
import { ReactComponent as HomeIcon } from "../../media/svg/home.svg";
import { ReactComponent as TrashIcon } from "../../media/svg/delete.svg";
import { ReactComponent as PageIcon } from "../../media/svg/page.svg";
import { ReactComponent as SettingsIcon } from "../../media/svg/settings.svg";

const Menu = () => {
  return (
    <div className={c.menuBody}>
      <Block isMain={true}>
        <Item icon={HomeIcon}>Главная страница</Item>
        <Item icon={SearchIcon}>Поиск</Item>
        <Item icon={TrashIcon}>Корзина</Item>
      </Block>
      <Block title="Избранное">
        <Item icon={PageIcon}>Лекция</Item>
      </Block>
      <Block title="Все конспекты">
        <Item icon={PageIcon}>Конспект 1</Item>
        <Item icon={PageIcon}>Конспект 2</Item>
        <Item icon={PageIcon}>Конспект 3</Item>
      </Block>
      <Block>
        <Item icon={SettingsIcon}>Настройки</Item>
      </Block>
    </div>
  );
};

export default React.memo(Menu);
