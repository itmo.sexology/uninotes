import React from "react";
import c from "./style.module.sass";

import { ReactComponent as LogoImage } from "../../media/svg/logo.svg";
import { ReactComponent as EyeOpenedIcon } from "../../media/svg/eye_opened.svg";
import { ReactComponent as EyeClosedIcon } from "../../media/svg/eye_closed.svg";

const Auth = () => {
  const [isVisible, setIsVisible] = React.useState(false);
  const [login, setLogin] = React.useState("");
  const [password, setPassword] = React.useState("");

  const EyeComponent = isVisible ? EyeOpenedIcon : EyeClosedIcon;

  return (
    <>
      <div className={c.topMenu}>
        <span className={c.logo}>
          <LogoImage />
        </span>
      </div>
      <div className={c.content}>
        <div className={c.background} />
        <div className={c.authWindow}>
          <h1 className={c.title}>Вход</h1>
          <div className={c.inputContainer}>
            <input
              type="text"
              placeholder="Логин"
              maxLength={22}
              className={c.input}
            />
          </div>
          <div className={c.inputContainer}>
            <input
              type={isVisible ? "text" : "password"}
              placeholder="Пароль"
              maxLength={13}
              className={c.input}
            />
            <EyeComponent
              className={c.passwordIcon}
              onClick={() => {
                setIsVisible((value) => !value);
              }}
            />
          </div>

          <button className={c.enter}>Войти</button>
        </div>
      </div>
    </>
  );
};

export default React.memo(Auth);
