import React from "react";
import classNames from "classnames";

import c from "./style.module.sass";

import Block from "../../components/Block";
import Menu from "../../components/Menu";

import { ReactComponent as UndoIcon } from "../../media/svg/undo.svg";
import { ReactComponent as RedoIcon } from "../../media/svg/redo.svg";
import { ReactComponent as ShareIcon } from "../../media/svg/ios_share.svg";
import { ReactComponent as LogoImage } from "../../media/svg/logo.svg";

const Notes = () => {
  const [notes, setNotes] = React.useState([
    { id: 1, type: "h1", value: "Lorem Ipsum" },
    { id: 2, type: "h3", value: "What is Lorem Ipsum?" },
    {
      id: 4,
      type: "p",
      value:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    },
    { id: 5, type: "h3", value: "Why do we use it?" },
    {
      id: 6,
      type: "p",
      value:
        "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
    },
    { id: 7, type: "h3", value: "Where does it come from?" },
    {
      id: 8,
      type: "p",
      value:
        "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of ",
    },
    {
      id: 9,
      type: "img",
      value:
        "https://tyriar.gallerycdn.vsassets.io/extensions/tyriar/lorem-ipsum/1.3.1/1640026564395/Microsoft.VisualStudio.Services.Icons.Default",
    },
  ]);

  return (
    <div className={c.panels}>
      <div className={c.menu}>
        <span className={c.logo}>
          <LogoImage />
        </span>
        <Menu />
      </div>
      <div className={c.workspace}>
        <div className={c.header}>
          <div className={c.headerLeft}>
            <div className={c.actionControl}>
              <span className={c.icon}>
                <UndoIcon />
              </span>
              <span className={classNames(c.icon, c.inactiveIcon)}>
                <RedoIcon />
              </span>
            </div>
            <span className={c.title}>Title</span>
          </div>
          <div className={c.headerRight}>
            <span className={c.icon}>
              <ShareIcon />
            </span>
          </div>
        </div>
        <div className={c.content}>
          {notes.map((block) => (
            <Block {...block} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default React.memo(Notes);
