from dataclasses import asdict, dataclass
from datetime import datetime as dt
from lib2to3.pytree import Base
from typing import Optional


@dataclass
class BaseResult:
    title: str
    message: str
    code: int = 200
    payload: Optional[dict] = None

    @property
    def timestamp(self):
        return int(dt.utcnow().timestamp())

    def asdict(self):
        return asdict(
            self, dict_factory=lambda x: {k: v for (k, v) in x if v is not None}
        ) | {"timestamp": self.timestamp}


@dataclass
class BaseError(BaseResult):
    type: str = "base"
    code: int = 400
