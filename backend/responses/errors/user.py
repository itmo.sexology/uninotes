from responses.base import BaseError

UserExistError = BaseError(
    title="User already exists", message="User already exist", type="user.exist"
)
